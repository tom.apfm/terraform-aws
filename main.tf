# --- terraform-aws/main.tf ---

provider "aws" {
  region = "${var.aws_region}"
}

# -- NOTE: these values are all hard coded and will not work if unless hardcoded values are adjusted
terraform {
  backend "s3" {
    bucket = "la-terraform-crunchy-bacon"
    key    = "terraform/terraform.tfstate"
    region = "us-east-1"
  }
}

# Deploy Storage Resource
module "storage" {
  source       = "./storage"
  project_name = "${var.project_name}"
}

# Deploy Networking Resources

module "networking" {
  source       = "./networking"
  vpc_cidr     = "${var.vpc_cidr}"
  public_cidrs = "${var.public_cidrs}"
  accessip     = "${var.accessip}"
}

# Deploy Compute Resources

module "compute" {
  source          = "./compute"
  instance_count  = "${var.instance_count}"
  key_name        = "${var.key_name}"
  public_key_path = "${var.public_key_path}"
  instance_type   = "${var.server_instance_type}"
  subnets         = "${module.networking.public_subnets}"
  security_group  = "${module.networking.public_sg}"
  subnet_ips      = "${module.networking.subnet_ips}"
}
