# Install terraform

```
sudo curl -O https://releases.hashicorp.com/terraform/0.11.5/terraform_0.11.5_linux_amd64.zip
sudo apt-get install unzip
sudo mkdir /bin/terraform
sudo unzip terraform_0.11.5_linux_amd64.zip -d /usr/local/bin/
terraform --version
```

# Clone git repo

```
git clone https://github.com/linuxacademy/content-terraform
```

# add to main.tf to make docker work

```
provider "docker" {
  version = "1.1"
}
```

# Cloud9

> **NOTE:** After recent changes to cloud sandboxes in linux academy, cloud9 only works in `us-east-1`

clone this repo into cloud9

```
git clone https://gitlab.com/tom.apfm/terraform-aws.git
```

> **NOTE:** `Username for 'https://gitlab.com'` = `tom.apfm`

Install terraform

```
sudo curl -O https://releases.hashicorp.com/terraform/0.11.5/terraform_0.11.5_linux_amd64.zip
sudo unzip terraform_0.11.5_linux_amd64.zip -d /usr/local/bin/
terraform --version
```

Add ssh key for terraform to use

```
ssh-keygen
```

Create s3 bucket to store terraform state files in.
Cerate a dir called `terraform` in the new s3 bucket
